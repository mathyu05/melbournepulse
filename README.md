# MelbournePulse

Realtime map showing all of Melbourne's Trams, Trains, and Buses.

## Website

<enter website here...>

## Technologies

* Node.js
* PTV's Transport API

## Data Structures

* Directed Weighted Graph for each direction of each route (bus, tram, and train)

## Goals

* Implement Trams
* Implement Trains
* Implement Buses

## Problems

* Work out how to smoothly and accurately move the marker based on time to arrival.