var http = require('http');
var url = require('url');

var dt = require('./FirstModule');

var hostname = '127.0.0.1';
var port = 3000;

var server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/html');
  
  var q = url.parse(req.url, true).query;
  var txt = q.year + " " + q.month;
  res.write(txt);

  res.write("The date and time are currently: " + dt.myDateTime() + "\n");
  res.write("Hello World!\n");
  res.end();
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});